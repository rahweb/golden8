<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="rg detail">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="card">
                        <div class="container-fliud">
                            <div class="wrapper row">
                                <div class="preview col-md-6 col-xs-12">
                                    <div class="preview-pic tab-content">
                                        <div class="tab-pane active" id="pic-1">
                                            <img class="pop" src="images/lg3.jpg" />
                                        </div>
                                        <div class="tab-pane" id="pic-2">
                                            <img class="pop" src="images/lg3.jpg" />
                                        </div>
                                        <div class="tab-pane" id="pic-3">
                                            <img class="pop" src="images/lg3.jpg" />
                                        </div>
                                        <div class="tab-pane" id="pic-4">
                                            <img class="pop" src="images/lg3.jpg" />
                                        </div>
                                    </div>
                                    <ul class="preview-thumbnail nav nav-tabs">
                                        <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="images/lg3.jpg" /></a></li>
                                        <li><a data-target="#pic-2" data-toggle="tab"><img src="images/lg3.jpg" /></a></li>
                                        <li><a data-target="#pic-3" data-toggle="tab"><img src="images/lg3.jpg" /></a></li>
                                        <li><a data-target="#pic-4" data-toggle="tab"><img src="images/lg3.jpg" /></a></li>
                                    </ul>
                                </div>
                                <div class="details col-md-6">
                                    <div class="row top">
                                        <div class="col-lg-9">
                                            <h5 class="product-title">دی وی دی کنکورد DV3670</h5>
                                            <span class="review-no">مدل : DV3670</span>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="rating">
                                                        <input type="radio" id="star10" name="rating" value="10" /><label for="star10" title="Rocks!">5 stars</label>
                                                        <input type="radio" id="star9" name="rating" value="9" /><label for="star9" title="Rocks!">4 stars</label>
                                                        <input type="radio" id="star8" name="rating" value="8" /><label for="star8" title="Pretty good">3 stars</label>
                                                        <input type="radio" id="star7" name="rating" value="7" /><label for="star7" title="Pretty good">2 stars</label>
                                                        <input type="radio" id="star6" name="rating" value="6" /><label for="star6" title="Meh">1 star</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <span class="review-no">رتبه بندی :</span>
                                        </div>
                                    </div>
                                    <div class="row product-body">
                                        <div class="col-md right">
                                            <h6 class="price">مشخصات فنی :</h6>
                                            <br>
                                            <ul>
                                                <li>طول : <span>360mm</span></li>
                                                <li>عرض : <span>340mm</span></li>
                                                <li>رنگ : <span>مشکی</span></li>
                                                <li>نوع دستگاه صوتی و تصویری : <span>DVD</span></li>
                                            </ul>
                                        </div>
                                        <div class="col-md left">
                                            <h6 class="price">فایلهای توضیحات محصول :</h6>
                                            <br>
                                            <ul>
                                                <li>طول : <span>360mm</span></li>
                                                <li>عرض : <span>340mm</span></li>
                                                <li>رنگ : <span>مشکی</span></li>
                                                <li>نوع دستگاه صوتی و تصویری : <span>DVD</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-8 prices">
                                            <label>قیمت برای شما:</label>
                                            <span>1,600,000 ریال</span>
                                        </div>
                                        <div class="col-lg-4 quantity">
                                            <label style="position: absolute;margin:16% 3%;">تعداد :</label>
                                            <input class="form-control count" type="number" name="quantity" value="1" min="1" placeholder="تعداد" style="text-align: center;margin-top:15%">
                                            <div class="quantity-nav">
                                                <div class="quantity-button quantity-up">
                                                    <i class="fa fa-angle-up"></i>
                                                </div>
                                                <div class="quantity-button quantity-down">
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row pr">
                        <?php include("blocks/baner.php");?>
                        <div class="row">
                            <div class="col-md-12">
                                <h5>محصولات مرتبط</h5>
                                <hr>
                                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="0">
                                    <!-- Carousel indicators -->
                                    <ol class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                    </ol>   
                                    <!-- Wrapper for carousel items -->
                                    <div class="carousel-inner">
                                        <div class="item carousel-item active">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Apple iPad</h4>
                                                            <p class="item-price"><strike>$400.00</strike> <span>$369.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Sony Headphone</h4>
                                                            <p class="item-price"><strike>$25.00</strike> <span>$23.99</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>		
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Macbook Air</h4>
                                                            <p class="item-price"><strike>$899.00</strike> <span>$649.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>								
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Nikon DSLR</h4>
                                                            <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item carousel-item">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Apple iPhone</h4>
                                                            <p class="item-price"><strike>$369.00</strike> <span>$349.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Canon DSLR</h4>
                                                            <p class="item-price"><strike>$315.00</strike> <span>$250.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Google Pixel</h4>
                                                            <p class="item-price"><strike>$450.00</strike> <span>$418.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>	
                                                <div class="col-sm-3">
                                                    <div class="thumb-wrapper">
                                                        <div class="img-box">
                                                            <img src="https://image.ibb.co/g0CAPp/ipad.jpg" class="img-responsive img-fluid" alt="">
                                                        </div>
                                                        <div class="thumb-content">
                                                            <h4>Apple Watch</h4>
                                                            <p class="item-price"><strike>$350.00</strike> <span>$330.00</span></p>
                                                            <a href="#" class="btn btn-primary">اضافه به لیست خرید</a>
                                                        </div>						
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Carousel controls -->
                                    <a class="carousel-control left-t carousel-control-prev" href="#myCarousel" data-slide="prev">
                                        <img src="images/right-arrow.png" alt="">
                                    </a>
                                    <a class="carousel-control right carousel-control-next" href="#myCarousel" data-slide="next">
                                        <img src="images/left-arrow.png" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>