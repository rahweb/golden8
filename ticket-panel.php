<html lang="en">
    <?php include("blocks/head.php");?>
    <body class="background">
        <?php include("blocks/menu.php");?>
        <div class="cp ticket-panel back">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <?php include("panel/side-panel.php");?>
                    </div>
                    <div class="col-lg-9 col-md-9 well">
                        <?php include("panel/top-panel.php");?>
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>شماره پیام</th>
                                            <th>عنوان پیام</th>
                                            <th>وضعیت</th>
                                            <th>نوع</th>
                                            <th>تاریخ</th>
                                            <th>پاسخ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>163</td>
                                            <td>اصلاحات سایت</td>
                                            <td>
                                                <center>
                                                    <span class="label label-success">پاسخ داده شده</span>
                                                </center>
                                            </td>
                                            <td>
                                                <center>
                                                    <span class="label label-success">امور آموزش شرکت</span>
                                                </center>
                                            </td>
                                            <td>۱۳۹۷/۰۹/۰۷ ۱۱:۳۶</td>
                                            <td>
                                                <center>
                                                    <a href="https://atlasme.ir/panel/orders/factor/3315" data-toggle="tooltip" data-original-title="مشاهده فاکتور" class="btn btn-warning  btn-xs">
                                                        مشاهده فاکتور 
                                                    </a>
                                                </center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>