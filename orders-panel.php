<html lang="en">
    <?php include("blocks/head.php");?>
    <body class="background">
        <?php include("blocks/menu.php");?>
        <div class="cp orders-panel back">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <?php include("panel/side-panel.php");?>
                    </div>
                    <div class="col-lg-9 col-md-9 well">
                        <?php include("panel/top-panel.php");?>
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>کد</th>
                                            <th>شماره فاکتور</th>
                                            <th>مبلغ پرداختی</th>
                                            <th>وضعیت فاکتور</th>
                                            <th>روش تحویل</th>
                                            <th>تعداد</th>
                                            <th>تاریخ</th>
                                            <th>عملیات</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>4705</td>
                                            <td>3315</td>
                                            <td>1,000</td>
                                            <td>پرینت شده</td>
                                            <td>حضوری</td>
                                            <td>1</td>
                                            <td>۱۳۹۷/۰۹/۰۷ ۱۱:۳۶</td>
                                            <td>
                                                <center>
                                                    <a href="https://atlasme.ir/panel/orders/factor/3315" data-toggle="tooltip" data-original-title="مشاهده فاکتور" class="btn btn-warning  btn-xs">
                                                        مشاهده فاکتور 
                                                    </a>
                                                </center>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>