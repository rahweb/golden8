<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="br product">
            <div class="container">
                <div class="search-banner text-white py-3 form-arka-plan" id="search-banner">
                    <div class="py-5" style="padding:3rem 0rem 0rem !important">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card acik-renk-form">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md">
                                                <p>دسته بندی اصلی</p>
                                                <div class="form-group ">
                                                    <select id="iller" class="form-control" >
                                                        <option selected>همه دسته ها</option>
                                                        <option>İstanbul</option>
                                                        <option>Ankara</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md">
                                                <p>دسته بندی اصلی</p>
                                                <div class="form-group ">
                                                    <select id="iller" class="form-control" >
                                                        <option selected>همه دسته ها</option>
                                                        <option>İstanbul</option>
                                                        <option>Ankara</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md">
                                                <p>دسته بندی فرعی</p>
                                                <div class="form-group ">
                                                    <select id="ilceler" class="form-control" >
                                                        <option selected>همه دسته ها</option>
                                                        <option>BMW</option>
                                                        <option>Audi</option>
                                                        <option>Maruti</option>
                                                        <option>Tesla</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md">
                                                <p>برندها</p>
                                                <div class="form-group ">
                                                    <select id="arac-turu" class="form-control" >
                                                        <option selected>همه برندها</option>
                                                        <option>Otobüs</option>
                                                        <option>Minibüs</option>
                                                        <option>Karavan</option>
                                                        <option>Vito</option>
                                                        <option>Limuzin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md">
                                                <p>مدل</p>
                                                <div class="form-group ">
                                                    <select id="arac-turu" class="form-control" >
                                                        <option selected>همه مدل ها</option>
                                                        <option>Otobüs</option>
                                                        <option>Minibüs</option>
                                                        <option>Karavan</option>
                                                        <option>Vito</option>
                                                        <option>Limuzin</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md">
                                                <button type="button" class="btn btn-warning  pl-5 pr-5" style="margin-top:27%;font-family:Mj_Dinar;background:#b78737;color:#fff;border-color:#b78737;border-radius: 0%">جستجو</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="ads">
                    <!-- Category Card -->
                    <div class="col-md-3">
                        <div class="card rounded">
                            <div class="card-image">
                                <!-- <span class="card-notify-badge">Low KMS</span> -->
                                <a href="">
                                    <span class="card-notify-year">
                                        <span class="flaticon flaticon-supermarket"></span>
                                    </span>
                                </a>
                                <img class="img-fluid" src="https://imageonthefly.autodatadirect.com/images/?USER=eDealer&PW=edealer872&IMG=USC80HOC011A021001.jpg&width=440&height=262" alt="Alternate Text" />
                            </div>
                            <!-- <div class="card-image-overlay m-auto">
                                <span class="card-detail-badge">Used</span>
                                <span class="card-detail-badge">$28,000.00</span>
                                <span class="card-detail-badge">13000 Kms</span>
                            </div> -->
                            <div class="card-body text-center">
                                <div class="ad-title m-auto">
                                    <h5>DW-T1309 S ظرفشویی رومیزی سام</h5>
                                </div>
                                <br>
                                <h6>45/000/000 ریال</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card rounded">
                            <div class="card-image">
                                <!-- <span class="card-notify-badge">Fully-Loaded</span> -->
                                <a href="">
                                    <span class="card-notify-year">
                                        <span class="flaticon flaticon-supermarket"></span>
                                    </span>
                                </a>
                                <img class="img-fluid" src="https://imageonthefly.autodatadirect.com/images/?USER=eDealer&PW=edealer872&IMG=CAC80HOC021B121001.jpg&width=440&height=262" alt="Alternate Text" />
                            </div>
                            <!-- <div class="card-image-overlay m-auto">
                                <span class="card-detail-badge">Used</span>
                                <span class="card-detail-badge">$28,000.00</span>
                                <span class="card-detail-badge">13000 Kms</span>
                            </div> -->
                            <div class="card-body text-center">
                                <div class="ad-title m-auto">
                                    <h5>DW-T1309 S ظرفشویی رومیزی سام</h5>
                                </div>
                                <br>
                                <h6>45/000/000 ریال</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card rounded">
                            <div class="card-image">
                                <!-- <span class="card-notify-badge">Price Reduced</span> -->
                                <a href="">
                                    <span class="card-notify-year">
                                        <span class="flaticon flaticon-supermarket"></span>
                                    </span>
                                </a>
                                <img class="img-fluid" src="https://imageonthefly.autodatadirect.com/images/?USER=eDealer&PW=edealer872&IMG=USC80HOC091A021001.jpg&width=440&height=262" alt="Alternate Text" />
                            </div>
                            <!-- <div class="card-image-overlay m-auto">
                                <span class="card-detail-badge">Used</span>
                                <span class="card-detail-badge">$22,000.00</span>
                                <span class="card-detail-badge">8000 Kms</span>
                            </div> -->
                            <div class="card-body text-center">
                                <div class="ad-title m-auto">
                                    <h5>DW-T1309 S ظرفشویی رومیزی سام</h5>
                                </div>
                                <br>
                                <h6>45/000/000 ریال</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="card rounded">
                            <div class="card-image">
                                <!-- <span class="card-notify-badge">Price Reduced</span> -->
                                <a href="">
                                    <span class="card-notify-year">
                                        <span class="flaticon flaticon-supermarket"></span>
                                    </span>
                                </a>
                                <img class="img-fluid" src="https://imageonthefly.autodatadirect.com/images/?USER=eDealer&PW=edealer872&IMG=USC80HOC091A021001.jpg&width=440&height=262" alt="Alternate Text" />
                            </div>
                            <!-- <div class="card-image-overlay m-auto">
                                <span class="card-detail-badge">Used</span>
                                <span class="card-detail-badge">$22,000.00</span>
                                <span class="card-detail-badge">8000 Kms</span>
                            </div> -->
                            <div class="card-body text-center">
                                <div class="ad-title m-auto">
                                    <h5>DW-T1309 S ظرفشویی رومیزی سام</h5>
                                </div>
                                <br>
                                <h6>45/000/000 ریال</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>