<!-- footer ================================================== --> 
<section class="action-sec">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <ul>
                    <li><span class="flaticon flaticon-phone-call"></span>شماره تماس : <span>۰۲۱۴۴۵۵۶۶۷۷</span></li>
                    <li><span class="flaticon flaticon-phone-call"></span>شماره تماس : <span>۰۲۱۴۴۵۵۶۶۷۷</span></li>
                    <li><span class="flaticon flaticon-phone-call"></span>شماره تماس : <span>۰۲۱۴۴۵۵۶۶۷۷</span></li>
                    <li><span class="flaticon flaticon-phone-call"></span>شماره تماس : <span>۰۲۱۴۴۵۵۶۶۷۷</span></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <ul>
                    <li><span class="flaticon flaticon-envelope"></span>ایمیل : <span>info@golden8.ir</span></li>
                    <li><span class="flaticon flaticon-telegram"></span>ایمیل : <span>info@golden8.ir</span></li>
                    <li><span class="flaticon flaticon-instagram"></span>ایمیل : <span>info@golden8.ir</span></li>
                    <li><span class="flaticon flaticon-whatsapp"></span>ایمیل : <span>info@golden8.ir</span></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <ul>
                    <li><span class="flaticon flaticon-placeholder"></span>آدرس دفترمرکزی : <span>تستی تستی تستی</span></li>
                    <li><span class="flaticon flaticon-placeholder"></span>آدرس کارخانه : <span>تستی تستی تستی </span></li>
                </ul>
            </div>
        </div>
    </div>
</section>