<div class="select-t">
    <section class="search-banner text-white py-3 form-arka-plan" id="search-banner">
        <div class="container py-5 my-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="card acik-renk-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md">
                                    <p>دسته بندی اصلی</p>
                                    <div class="form-group ">
                                        <select id="iller" class="form-control" >
                                            <option selected>همه دسته ها</option>
                                            <option>İstanbul</option>
                                            <option>Ankara</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <p>دسته بندی اصلی</p>
                                    <div class="form-group ">
                                        <select id="iller" class="form-control" >
                                            <option selected>همه دسته ها</option>
                                            <option>İstanbul</option>
                                            <option>Ankara</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <p>دسته بندی فرعی</p>
                                    <div class="form-group ">
                                        <select id="ilceler" class="form-control" >
                                            <option selected>همه دسته ها</option>
                                            <option>BMW</option>
                                            <option>Audi</option>
                                            <option>Maruti</option>
                                            <option>Tesla</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <p>برندها</p>
                                    <div class="form-group ">
                                        <select id="arac-turu" class="form-control" >
                                            <option selected>همه برندها</option>
                                            <option>Otobüs</option>
                                            <option>Minibüs</option>
                                            <option>Karavan</option>
                                            <option>Vito</option>
                                            <option>Limuzin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <p>مدل</p>
                                    <div class="form-group ">
                                        <select id="arac-turu" class="form-control" >
                                            <option selected>همه مدل ها</option>
                                            <option>Otobüs</option>
                                            <option>Minibüs</option>
                                            <option>Karavan</option>
                                            <option>Vito</option>
                                            <option>Limuzin</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <button type="button" class="btn btn-warning  pl-5 pr-5">جستجو</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>