<div class="nav-side-menu">
    <div class="brand">
        <br>
        <div class="img">
            <img src="images/man_1074308.png" alt="">
        </div>
        <span>خوش آمدید,</span>
        <p style="font-size:1.5vw">نام و نام خانوادگی</p>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
    <div class="menu-list">
        <ul id="menu-content" class="menu-content collapse out">
            <li>
                <a href="#">
                    <i class="fa fa-user fa-lg"></i>
                    مشاهده مشخصات
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-user fa-lg"></i>
                    مشاهده مدارک
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-users fa-lg"></i>
                    سفارشات
                </a>
            </li>
            <!-- <li  data-toggle="collapse" data-target="#lst1" class="collapsed active">
                <a href="#">
                    <i class="fa fa-dashboard fa-lg"></i>
                    فروشگاه ها و محصولات 
                    <span class="arrow"></span>
                </a>
            </li>
            <ul class="sub-menu collapse" id="lst1">
                <li class="">
                    <a href="#">رسته ها</a>
                </li>
                <li class="">
                    <a href="#">فروشگاه ها</a>
                </li>
                <li class="">
                    <a href="#">مشخصات</a>
                </li>
            </ul> -->
            <!-- <li  data-toggle="collapse" data-target="#products" class="collapsed active">
                <a href="#">
                    <i class="fa fa-search fa-lg"></i>
                    محتوای سایت 
                    <span class="arrow"></span>
                </a>
            </li>
            <ul class="sub-menu collapse" id="products">
                <li>
                    <a href="#">
                        اسلایدرها
                    </a>
                </li>
                <li>
                    <a href="#">
                        بنرها
                    </a>
                </li>
                <li>
                    <a href="#">
                        زبان ها
                    </a>
                </li>
                <li>
                    <a href="#">
                        تنظیمات سایت
                    </a>
                </li>
            </ul> -->
            <!-- <li  data-toggle="collapse" data-target="#Eval" class="collapsed active">
                <a href="#">
                    <i class="fa fa-search fa-lg"></i>
                    سفارش ها 
                    <span class="arrow"></span>
                </a>
            </li>
            <ul class="sub-menu collapse" id="Eval">
                <li>
                    <a href="#">همه ی فاکتورها</a>
                </li>
            </ul> -->
            <!-- <li  data-toggle="collapse" data-target="#config" class="collapsed active">
                <a href="#">
                    <i class="fa fa-search fa-lg"></i>
                    کاربران و سطح دسترسی 
                    <span class="arrow"></span>
                </a>
            </li>
            <ul class="sub-menu collapse" id="config">
                <li>
                    <a href="#">لیست کاربر</a>
                </li>
                <li>
                    <a href="#">مدیریت سطح دسترسی</a>
                </li>
            </ul> -->
            <br>
        </ul>
    </div>
</div>