<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <div class="rg register">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 ">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">ثبت نام شخص  حقیقی</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">ثبت نام شخص حقوقی</a>
                            </div>
                        </nav>
                        <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                مشخصات:
                                <hr>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>نام : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>نام خانوادگی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>کدملی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>استان : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>شهر : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>آدرس : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>تلفن فروشگاه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>تلفن همراه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>رمز عبور : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                مدارک:
                                <hr>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>کپی کارت ملی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>مجوز فروشگاه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>رمز عبور : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="container">
                                        <div class="buying-selling-group" id="buying-selling-group" data-toggle="buttons">
                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option1" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>
                                        
                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>

                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option3" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button type="button" class="btn btn-warning  pl-5 pr-5">ثبت نام</button>
                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                مشخصات:
                                <hr>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>نام : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>نام خانوادگی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>کدملی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>استان : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>شهر : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>آدرس : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>تلفن فروشگاه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>تلفن همراه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>رمز عبور : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                مدارک:
                                <hr>
                                <div class="row bac">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>کپی کارت ملی : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>مجوز فروشگاه : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4">
                                                <p>رمز عبور : </p>
                                            </div>
                                            <div class="col-lg-8 col-md-8">
                                            <input type="file" name="import_file" id="input_import_file">                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row bac">
                                    <div class="container">
                                        <div class="buying-selling-group" id="buying-selling-group" data-toggle="buttons">
                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option1" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>
                                        
                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option2" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>

                                            <label class="btn btn-default buying-selling">
                                                <input type="radio" name="options" id="option3" autocomplete="off">
                                                <span class="radio-dot"></span>
                                                <span class="buying-selling-word">تستی</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <button type="button" class="btn btn-warning  pl-5 pr-5">ثبت نام</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>