<html lang="en">
    <?php include("blocks/head.php");?>
    <body class="background">
        <?php include("blocks/menu.php");?>
        <div class="cp Specifications-panel back">
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <?php include("panel/side-panel.php");?>
                    </div>
                    <div class="col-lg-9 col-md-9 well">
                        <?php include("panel/top-panel.php");?>
                        <div class="container">
                            <div class="row">
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td>نام :</td>
                                            <td>حسین</td>
                                        </tr>
                                        <tr>
                                            <td>نام خانوادگی :</td>
                                            <td>سعیدی</td>
                                        </tr>
                                        <tr>
                                            <td>کد ملی :</td>
                                            <td>۰۰۱۲۳۳۴۶۷۸</td>
                                        </tr>
                                        <tr>
                                            <td>استان :</td>
                                            <td>تهران</td>
                                        </tr>
                                        <tr>
                                            <td>شهر :</td>
                                            <td>تهران</td>
                                        </tr>
                                        <tr>
                                            <td>آدرس :</td>
                                            <td>تهران-تهران-تهران-تهران</td>
                                        </tr>
                                        <tr>
                                            <td>شماره همراه :</td>
                                            <td>۰۹۱۲۳۳۴۴۵۵۶</td>
                                        </tr>
                                        <tr>
                                            <td>تلفن فروشگاه :</td>
                                            <td>۰۲۱۴۴۵۵۶۷۸۹</td>
                                        </tr>
                                        <tr>
                                            <td>رمز عبور :</td>
                                            <td>۲۳۴۵۶۱۷</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>